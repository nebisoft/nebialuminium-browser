.PHONY: build

build:
	rm -rf ./build/exe.nebios/
	mkdir -p ./build/exe.nebios/unpacked
	cp ./NebiAluminium.py ./build/exe.nebios/unpacked
	cp -r ./assets ./build/exe.nebios/unpacked
	cp -r ./lib ./build/exe.nebios/unpacked
	cp -r ./engine ./build/exe.nebios/unpacked
	python3 -m compileall ./build/exe.nebios/unpacked
	python3 -m venv ./build/exe.nebios/unpacked/venv
	./build/exe.nebios/unpacked/venv/bin/pip install requests pyside6 adblockparser
	echo '#!/bin/bash' > ./build/exe.nebios/unpacked/app.exec
	echo 'export PATH=$$PWD/venv/bin:$$PATH' >> ./build/exe.nebios/unpacked/app.exec
	echo 'bash $$PWD/venv/bin/activate' >> ./build/exe.nebios/unpacked/app.exec
	echo 'python3 ./NebiAluminium.py $$@' >> ./build/exe.nebios/unpacked/app.exec
	chmod +x ./build/exe.nebios/unpacked/app.exec
	cp ./icon.png ./build/exe.nebios/NebiAluminium.png
	cp ./NebiAluminium.ninf ./build/exe.nebios/NebiAluminium.ninf
	exec2napp ./build/exe.nebios/unpacked/ ./build/exe.nebios/NebiAluminium.ninf ./build/exe.nebios/NebiAluminium.napp
