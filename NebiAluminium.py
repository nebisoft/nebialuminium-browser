import os
import sys

if getattr(sys, "frozen", False):
    # The application is frozen
    datadir = os.path.dirname(sys.executable)
else:
    # The application is not frozen
    datadir = os.path.dirname(__file__)

# Now import using the appropriate path
import_path = datadir

if import_path not in sys.path:
    sys.path.append(import_path)

import json
import subprocess
from datetime import datetime
import threading
from urllib.parse import urlparse
import requests
from PySide6.QtPrintSupport import *
from PySide6.QtWebEngineWidgets import *
from PySide6.QtNetwork import *
from lib.QtNebiStylestrip.NebiClient import *
from lib.QtNebiStylestrip.EzFont import *
from lib.libSearchEngine import *
from PySide6.QtWebEngineCore import *
from adblockparser import AdblockRules
import lib.AluminiumEngine as AluminiumEngine

APP_VERSION = "3.0"

DEBUG_PORT = '5360'
DEBUG_URL = 'http://127.0.0.1:%s' % DEBUG_PORT
os.environ['QTWEBENGINE_REMOTE_DEBUGGING'] = DEBUG_PORT
        
BLOCK_LIST = AdblockRules(open("engine/blocker/filters.txt", encoding="utf8").readlines())

class AdBlockInterceptor(QWebEngineUrlRequestInterceptor):
    def interceptRequest(self, info):
        global BLOCK_LIST
        url = info.requestUrl().toString()
        if BLOCK_LIST.should_block(url):
            print("[AluminiumEngine] Blocked", url)
            info.block(True)
    
    def SkipYtAds(self, browser):
        init_funcs = """
            const skip = () => {
                const buttons = document.getElementsByClassName('ytp-skip-ad-button');
                for (const button of buttons) {
                    button.click();
                }
                if (document.getElementsByClassName('ad-showing').length != 0) {
                    document.querySelector('video').currentTime = document.querySelector('video').duration;
                }
            };
            
            const hide = () => {
                const views_1 = document.getElementsByClassName('ytd-player-legacy-desktop-watch-ads-renderer');
                const views_2 = document.getElementsByTagName('ytd-ad-slot-renderer');
                
                // Convert HTMLCollection to an array and merge them
                const views = [...views_1, ...views_2];
                
                // Loop through the merged array and hide each element
                for (const view of views) {
                    view.style.display = "none";
                }
            };
        """
        browser.page().runJavaScript(init_funcs)
        browser.page().runJavaScript("setInterval(function() { skip(); hide(); }, 100);")
    
            
class AluminiumEngineProfile(QWebEngineProfile):
    def __init__(self, name):
        super().__init__(name)
        self.setPersistentCookiesPolicy(QWebEngineProfile.PersistentCookiesPolicy.ForcePersistentCookies)
        self.cookieStore().loadAllCookies()

class AluminiumEnginePage(QWebEnginePage):
    def __init__(self, prof, parent):
        super().__init__(prof, parent)
        self.parent = parent
        self.profile().downloadRequested.connect(self.downloadRequested)
        self.abi = AdBlockInterceptor()
        self.profile().setUrlRequestInterceptor(self.abi)
        
    def acceptNavigationRequest(self, url, navtype, isMainFrame):
        global BLOCK_LIST
        print("interceptRequest")
        requrl = url.toString()
        print("Load", requrl)
        if BLOCK_LIST.should_block(requrl):
            print(f"Blocked: {requrl}")
            return False
        else:
            print(f"Not Blocked: {requrl}")
            return True

    def downloadRequested(self, download):
        old_path = download.downloadFileName()
        suffix = QFileInfo(old_path).suffix()
        path, _ = QFileDialog.getSaveFileName(
            self.parent.parent, "Save File", old_path, "*." + suffix
        )
        if path:
            print("Download to: " + path)
            download.setDownloadDirectory(path.rsplit("/", 1)[0])
            download.setDownloadFileName(path.rsplit("/", 1)[1])
            download.accept()
            # download.isFinishedChanged.connect(lambda: self.onDownloadFinished(path.rsplit("/", 1)[1]))

    def createWindow(self, _type):
        page = AluminiumEnginePage(self.profile(), self.parent)
        page.urlChanged.connect(self.on_url_changed)
        return page

    @Slot(QUrl)
    def on_url_changed(self, url):
        page = self.sender()
        self.parent.parent.addNewTab(url.toString())
        page.deleteLater()


class AluminiumEngineView(QWebEngineView):
    def __init__(self, parent, *args, **kwargs):
        QWebEngineView.__init__(self, *args, **kwargs)
        self.parent = parent
        
    def contextMenuEvent(self, event):
        # Varsayılan bağlam menüsünü al
        default_menu = self.createStandardContextMenu()

        # Özel bir QAction oluştur
        toggle_dev_tools = QAction("Toggle developer tools...", self)
        toggle_dev_tools.triggered.connect(self.parent.window().toggleDevTools)

        # Özel QAction'yı bağlam menüsüne ekle
        default_menu.addSeparator()
        default_menu.addAction(toggle_dev_tools)

        # Bağlam menüsünü göster
        default_menu.exec_(event.globalPos())


class AluminiumTabBar(QTabBar):
    def __init__(self):
        super().__init__()
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)

    def tabSizeHint(self, index):
        size = QTabBar.tabSizeHint(self, index)
        try:
            w = self.width() / self.count()
            if w < 96:
                w = 96
            return QSize(w, size.height())
        except Exception as e:
            print("Error: " + str(e))
            return size


VALID_SCHEMES = {"http", "https", "ftp", "file", "data", "chrome", "about", "aluminium"}


class NebiAluminium(NebiClient):

    def __init__(self, isPrivate=False):
        super().__init__()

        icon_path = "./assets/com.nebisoftware.nebialuminium_512.png"
        app.setWindowIcon(QIcon(icon_path))

        self.settingsprof = QSettings('NebiSoft', 'NebiChat')
        self.resize(self.settingsprof.value("winSize", QSize(1150, 600)))
        self.move(self.settingsprof.value("winPos", QPoint(50, 50)))
        self.restoreState(self.settingsprof.value("winState", self.saveState()))
        self.search_engine = self.settingsprof.value("searchEngine", "SearXNG")

        # Init history & bookmarks
        self.isPrivate = isPrivate

        if not self.isPrivate:
            self.profile = AluminiumEngineProfile(f"Default")
        else:
            self.profile = AluminiumEngineProfile(f"Profile_Private")
            self.profile.setPersistentCookiesPolicy(QWebEngineProfile.PersistentCookiesPolicy.NoPersistentCookies)

        if self.isPrivate:
            self.profile.clearHttpCache()
            self.profile.clearAllVisitedLinks()
            self.headerBar.setBackgroundColor("#ff535353")

        self.non_visible_aluminium_scheme_pages = {
            "aluminium:new_tab",
            "aluminium:new_tab_priv",
            "aluminium:welcome"
        }

        self.TitleEnd = " · NebiAluminium"
        if self.isPrivate:
            self.TitleEnd = " · (Private) NebiAluminium"
        self.tabWidget = QTabWidget()
        self.tabWidget.setStyleSheet("background-color: white;")
        self.browser_tab_bar = AluminiumTabBar()
        self.tabWidget.setTabBar(self.browser_tab_bar)

        # Make the window transparent
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.setStyleSheet("background:transparent;")

        # Clear existing header title layout items
        while self.headerBar.header_title_layout.count():
            item = self.headerBar.header_title_layout.takeAt(0)
            widget = item.widget()
            if widget:
                widget.deleteLater()

        # Back button
        self.back_btn = QToolButton(self)
        self.back_btn.setIcon(QIcon.fromTheme("go-previous"))
        self.back_btn.setText("Back")
        self.back_btn.clicked.connect(lambda _: self.tabWidget.currentWidget().back())

        # Next button
        self.next_btn = QToolButton(self)
        self.next_btn.setIcon(QIcon.fromTheme("go-next"))
        self.next_btn.setText("Next")
        self.next_btn.clicked.connect(lambda _: self.tabWidget.currentWidget().forward())

        # Refresh button
        self.refresh_btn = QToolButton(self)
        self.refresh_btn.setIcon(QIcon.fromTheme("view-refresh"))
        self.refresh_btn.setText("Refresh")
        self.refresh_btn.clicked.connect(self.handleRefresh)

        # Expanding space
        spacer_1 = QWidget()
        spacer_1.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        # URL box
        self.url_box = QLineEdit()
        self.url_box.setPlaceholderText("Search something...")
        self.url_box.returnPressed.connect(self.handleSearchOrLoad)
        self.url_box.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.completer = QCompleter(self)
        self.completer.popup().setObjectName("completerPopup")
        self.completer.setCaseSensitivity(Qt.CaseInsensitive)
        self.completer.activated.connect(self.handleSuggestionActivated)
        self.url_box.setCompleter(self.completer)
        self.url_box.textChanged.connect(self.fetchSearchSuggestions)

        # Expanding space
        spacer_2 = QWidget()
        spacer_2.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        # Three buttons
        new_tab_button = QToolButton()
        new_tab_button.setIcon(QIcon.fromTheme("tab-new"))  # Use the appropriate theme icon name
        new_tab_button.setText("New Tab")
        new_tab_button.clicked.connect(self.newTab)

        # Library button
        library_btn = QToolButton(self)
        library_btn.setIcon(QIcon.fromTheme("folder"))
        library_btn.setText("Library")
        library_btn.clicked.connect(self.showLibMenu)

        menu_button = QToolButton()
        menu_button.setIcon(QIcon.fromTheme("open-menu-symbolic"))
        menu_button.setText("More Options")
        menu_button.clicked.connect(self.showMenu)

        self.headerBar.header_title_layout.addWidget(self.back_btn)
        self.headerBar.header_title_layout.addWidget(self.next_btn)
        self.headerBar.header_title_layout.addWidget(self.refresh_btn)
        self.headerBar.header_title_layout.addWidget(spacer_1, stretch=1)
        self.headerBar.header_title_layout.addWidget(self.url_box, stretch=3)
        self.headerBar.header_title_layout.addWidget(spacer_2, stretch=1)
        self.headerBar.header_title_layout.addWidget(new_tab_button)
        self.headerBar.header_title_layout.addWidget(library_btn)
        self.headerBar.header_title_layout.addWidget(menu_button)

        # Create a QTabWidget for tabs
        self.tabWidget.setDocumentMode(True)
        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(self.closeTab)
        self.tabWidget.currentChanged.connect(self.tabChanged)

        # Remove margins from the content layout
        self.content_layout.setContentsMargins(0, 0, 0, 0)
        self.content_layout.setSpacing(0)

        self.splitter = QSplitter(self)
        self.splitter.setOrientation(Qt.Orientation.Vertical)
        self.splitter.addWidget(self.tabWidget)
        self.splitter.setStyleSheet("background-color: rgba(255, 255, 255, 1);")
        self.inspector = None

        # Add the headerBar and tabWidget to the content layout
        self.content_layout.addWidget(self.splitter)

        # Add the initial tab
        self.newTab()

        # Set the content layout to the main content widget

        self.menu_bar.setMaximumSize(0, 0)

        self.file_menu = self.menu_bar.addMenu("File")
        self.edit_menu = self.menu_bar.addMenu("Edit")
        self.view_menu = self.menu_bar.addMenu("View")
        self.menu_bar_lib_menu = self.menu_bar.addMenu("Library")
        self.tools_menu = self.menu_bar.addMenu("Tools")
        self.help_menu = self.menu_bar.addMenu("Help")

        # Populate menus
        self.populateFileMenu()
        self.populateEditMenu()
        self.populateViewMenu()

    def populateFileMenu(self):
        new_tab_action = QAction("New Tab", self)
        new_tab_action.setShortcut(QKeySequence("Ctrl+T"))
        new_tab_action.triggered.connect(self.newTab)

        new_window_action = QAction("New Window", self)
        new_window_action.setShortcut(QKeySequence.New)
        new_window_action.triggered.connect(self.newWindow)

        new_private_window_action = QAction("New Private Window", self)
        new_private_window_action.setShortcut(QKeySequence("Ctrl+Shift+T"))
        new_private_window_action.triggered.connect(self.newPrivateWindow)

        close_tab_action = QAction("Close Tab", self)
        close_tab_action.setShortcut(QKeySequence.Close)
        close_tab_action.triggered.connect(lambda: self.closeTab(self.tabWidget.currentIndex()))

        save_page_action = QAction("Save Page", self)
        save_page_action.setShortcut(QKeySequence.Save)
        save_page_action.triggered.connect(self.savePage)

        open_file_action = QAction("Open File", self)
        open_file_action.setShortcut(QKeySequence.Open)
        open_file_action.triggered.connect(self.openFile)

        print_page_action = QAction("Print Page", self)
        print_page_action.setShortcut(QKeySequence.Print)
        print_page_action.triggered.connect(self.printPage)

        exit_action = QAction("Exit", self)
        exit_action.setShortcut(QKeySequence.Quit)
        exit_action.triggered.connect(self.closeWindow)

        self.file_menu.addAction(new_tab_action)
        self.file_menu.addAction(new_window_action)
        self.file_menu.addAction(new_private_window_action)
        self.file_menu.addSeparator()
        self.file_menu.addAction(close_tab_action)
        self.file_menu.addSeparator()
        self.file_menu.addAction(save_page_action)
        self.file_menu.addAction(open_file_action)
        self.file_menu.addAction(print_page_action)
        self.file_menu.addSeparator()
        self.file_menu.addAction(exit_action)

    def populateEditMenu(self):
        self.edit_menu.addAction(self.tabWidget.currentWidget().pageAction(QWebEnginePage.WebAction.Cut))
        self.edit_menu.addAction(self.tabWidget.currentWidget().pageAction(QWebEnginePage.WebAction.Copy))
        self.edit_menu.addAction(self.tabWidget.currentWidget().pageAction(QWebEnginePage.WebAction.Paste))
        self.edit_menu.addSeparator()
        self.edit_menu.addAction(self.tabWidget.currentWidget().pageAction(QWebEnginePage.WebAction.SelectAll))

    def populateViewMenu(self):
        self.view_menu.addAction("Zoom -10%", self.zoomOut, QKeySequence("Ctrl+-"))
        self.view_menu.addAction("Zoom +10%", self.zoomIn, QKeySequence("Ctrl+*"))
        self.view_menu.addAction("Reset zoom", self.resetZoom, QKeySequence("Ctrl+0"))

        self.view_menu.addSeparator()
        toggle_menu_bar_action = QAction("Toggle Menu Bar", self)
        toggle_menu_bar_action.setShortcut(QKeySequence("Alt+M"))
        toggle_menu_bar_action.triggered.connect(self.toggleMenuBar)

        toggle_fullscreen_action = QAction("Toggle Fullscreen", self)
        toggle_fullscreen_action.setShortcut(QKeySequence("F11"))
        toggle_fullscreen_action.triggered.connect(lambda: self.fullScreen())

        self.view_menu.addAction(toggle_menu_bar_action)
        self.view_menu.addAction(toggle_fullscreen_action)

    def printPage(self):
        current_tab_widget = self.tabWidget.currentWidget()
        if current_tab_widget:
            # Create a print dialog
            print_dialog = QPrintDialog(self)
            QSS.load_stylesheet(print_dialog, "lib/QtNebiStylestrip/qss/app.qss")

            if print_dialog.exec_() == QPrintDialog.Accepted:
                # If the user accepts the print dialog, print the current page
                current_tab_widget.page().print(print_dialog.printer())

    def toggleMenuBar(self):
        # Toggle the visibility of the menu bar
        menu_bar: QMenuBar = self.menu_bar
        if menu_bar.width() == 0:
            menu_bar.setMaximumSize(self.width(), self.height())
        else:
            menu_bar.setMaximumSize(0, 0)

    def newTab(self):
        if self.isPrivate == False:
            self.addNewTab("aluminium:new_tab")
        else:
            self.addNewTab("aluminium:new_tab_priv")

    def savePage(self):
        current_tab_widget: QWebEngineView = self.tabWidget.currentWidget()
        if current_tab_widget:
            file_dialog = QFileDialog(self)
            QSS.load_stylesheet(file_dialog, "lib/QtNebiStylestrip/qss/app.qss")
            file_dialog.setAcceptMode(QFileDialog.AcceptSave)
            file_dialog.setNameFilter("HTML Files (*.html);;All Files (*)")

            if file_dialog.exec_() == QFileDialog.Accepted:
                file_name = file_dialog.selectedFiles()[0]
                current_tab_widget.page().save(file_name, QWebEngineDownloadRequest.SavePageFormat.SingleHtmlSaveFormat)

    def openFile(self):
        file_dialog = QFileDialog(self)
        QSS.load_stylesheet(file_dialog, "lib/QtNebiStylestrip/qss/app.qss")
        file_dialog.setFileMode(QFileDialog.ExistingFile)
        file_dialog.setNameFilter("All Files (*)")

        if file_dialog.exec_() == QFileDialog.Accepted:
            file_name = file_dialog.selectedFiles()[0]

            # Open the selected file in a new tab
            self.addNewTab(f"file://{file_name}")

    def writeToFile(self, data, file_path):
        try:
            with open(file_path, 'w', encoding='utf-8') as file:
                file.write(data)
            print(f"Page saved to: {file_path}")
        except Exception as e:
            print(f"Error saving page: {e}")

    def showLibMenu(self):
        lib_menu = QMenu(self)
        QSS.load_stylesheet(lib_menu, "lib/QtNebiStylestrip/qss/app.qss")

        # Library menu
        history_submenu = lib_menu.addMenu("History")
        history_submenu.addAction("Show History", lambda: self.addNewTab("aluminium:history"))
        history_submenu.addAction("Clean History", self.cleanHistory)  # Implement this method

        bookmarks_submenu = lib_menu.addMenu("Bookmarks")
        bookmarks_submenu.addAction("Show Bookmarks",
                                    lambda: self.addNewTab("aluminium:bookmarks"))  # Implement this method
        bookmarks_submenu.addAction("Add Bookmark", self.addBookmark)  # Implement this method

        lib_menu.addAction("Downloads in this session", self.showDownloads)  # Implement this method

        lib_menu.exec_(self.mapToGlobal(self.headerBar.header_title_layout.itemAt(7).widget().pos()))

    def showMenu(self):
        menu_menu = QMenu(self)
        QSS.load_stylesheet(menu_menu, "lib/QtNebiStylestrip/qss/app.qss")

        # Menu menu
        menu_menu.addAction("New tab", self.newTab)
        menu_menu.addAction("New window", self.newWindow)
        menu_menu.addAction("New private window", self.newPrivateWindow)  # Implement this method
        menu_menu.addSeparator()

        zoom_submenu = menu_menu.addMenu("Zoom")
        zoom_submenu.addAction("Zoom -10%", self.zoomOut, QKeySequence("Ctrl+-"))
        zoom_submenu.addAction("Zoom +10%", self.zoomIn, QKeySequence("Ctrl+*"))
        zoom_submenu.addAction("Reset zoom", self.resetZoom, QKeySequence("Ctrl+0"))
        menu_menu.addAction("Fullscreen", lambda: self.fullScreen())

        menu_menu.addSeparator()
        menu_menu.addAction("Settings", self.showSettings)  # Implement this method
        menu_menu.addAction("About NebiAluminium", self.showAboutDialog)  # Implement this method
        menu_menu.addAction("Close", self.closeWindow)  # Implement this method

        menu_menu.exec_(self.mapToGlobal(self.headerBar.header_title_layout.itemAt(8).widget().pos()))

    def cleanHistory(self):
        global _browser_history_data
        global _browser_profile_dir

        history_file_path = os.path.join(_browser_profile_dir, "history.json")

        with open(history_file_path, "w") as file:
            json.dump([], file)

        with open(history_file_path, "r") as file:
            _browser_history_data = json.load(file)

    def addBookmark(self):
        # Implement the logic to add a bookmark
        pass

    def showDownloads(self):
        # Implement the logic to show downloads in this session
        pass

    def newWindow(self):
        # Create a new instance of NebiAluminium class for a new window
        new_window = NebiAluminium()
        new_window.show()

    def newPrivateWindow(self):
        new_window = NebiAluminium(isPrivate=True)
        new_window.show()

    def zoomIn(self):
        current_tab_widget = self.tabWidget.currentWidget()
        if current_tab_widget:
            current_tab_widget.setZoomFactor(current_tab_widget.zoomFactor() + 0.1)

    def zoomOut(self):
        current_tab_widget = self.tabWidget.currentWidget()
        if current_tab_widget:
            current_tab_widget.setZoomFactor(max(0.1, current_tab_widget.zoomFactor() - 0.1))

    def resetZoom(self):
        current_tab_widget = self.tabWidget.currentWidget()
        if current_tab_widget:
            current_tab_widget.setZoomFactor(1.0)

    def onFullscreen(self, request):
        isFullscreen = request.toggleOn()
        request.accept()
        if (isFullscreen == True):
            self.fullScreen(True)

        if (isFullscreen == False):
            self.fullScreen(False)

    def fullScreen(self, set=None):
        if set == None:
            if self.windowState() != Qt.WindowState.WindowFullScreen:
                self.headerBar.hide()
                self.showFullScreen()
            else:
                self.headerBar.show()
                self.showNormal()
        else:
            if set == True:
                self.headerBar.hide()
                self.showFullScreen()
            elif set == False:
                self.headerBar.show()
                self.showNormal()

    def showSettings(self):
        # Implement the logic to show settings
        pass

    def showAboutDialog(self):
        # Implement the logic to show the about dialog
        pass

    def closeWindow(self):
        self.close()
        pass

    def toggleDevTools(self):
        if self.inspector != None:
            self.tabWidget.currentWidget().page().setDevToolsPage(None)
            self.inspector.deleteLater()
            self.inspector = None
        else:
            self.inspector = QWebEngineView()
            opacity_effect = QGraphicsOpacityEffect(self)
            opacity_effect.setOpacity(0)
            self.inspector.setGraphicsEffect(opacity_effect)
            self.inspector.setMinimumHeight(128)
            self.inspector.setGeometry(0, 0, 1, 256)
            self.splitter.addWidget(self.inspector)
            self.tabWidget.currentWidget().page().setDevToolsPage(self.inspector.page())

    def addNewTab(self, url):
        # Create a new QWebEngineView for the tab
        webview = AluminiumEngineView(self.tabWidget)
        opacity_effect = QGraphicsOpacityEffect(self.tabWidget)
        opacity_effect.setOpacity(0)
        webview.setGraphicsEffect(opacity_effect)
        webview.setAttribute(Qt.WA_AcceptTouchEvents, True)

        webpage = AluminiumEnginePage(self.profile, webview)
        webpage.fullScreenRequested.connect(self.onFullscreen)
        webpage.featurePermissionRequested.connect(
            lambda url, feature: self.onFeaturePermissionRequested(url, feature, webview))

        webview.setPage(webpage)

        # Get the settings object
        settings = webview.settings()
        settings.setAttribute(QWebEngineSettings.WebAttribute.FullScreenSupportEnabled, True)
        settings.setAttribute(QWebEngineSettings.WebAttribute.LocalStorageEnabled, True)
        settings.setAttribute(QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True)
        settings.setAttribute(QWebEngineSettings.WebAttribute.ScreenCaptureEnabled, True)
        webview.urlChanged.connect(lambda _: self.url_box_update(webview))
        webview.titleChanged.connect(lambda _: self.titleChanged(webview))
        webview.loadStarted.connect(lambda: self.updateLoadingState(True))
        webview.loadFinished.connect(lambda: self.updateLoadingState(False))
        webview.loadFinished.connect(lambda: self.loadSchemeData(webview))
        webview.loadFinished.connect(lambda: webpage.abi.SkipYtAds(webview))

        if self.isPrivate == False:
            webview.loadFinished.connect(lambda: self.addHistoryDataFromWidget(webview))

        link = AluminiumEngine.Scheme.unapply(url)
        webview.load(QUrl(str(link)))

        # Add the webview to a new tab
        index = self.tabWidget.addTab(webview, "New Tab")

        # Set the current tab to the newly added tab
        self.tabWidget.setCurrentIndex(index)

        # Update the tab title when the page title changes
        webview.page().titleChanged.connect(lambda title: self.tabWidget.setTabText(index, title))

        if self.browser_tab_bar.count() > 1:
            self.browser_tab_bar.show()
            self.content.raise_()
        else:
            self.browser_tab_bar.hide()
            self.headerBar.raise_()

    def loadSchemeData(self, view: QWebEngineView):
        global _browser_profile_dir

        url = AluminiumEngine.Scheme.apply(view.url().toString()).replace("aluminium:", "", 1)
        if url.startswith("//"):
            url = url.replace("//", "root/", 1)
        elif url.startswith("/"):
            url = url.replace("/", "root/", 1)
        else:
            url = "root/" + url

        if url.startswith("root/history"):
            history_file_path = os.path.join(_browser_profile_dir, "history.json")
            with open(history_file_path, "r") as file:
                browser_history_data: list = json.load(file)

            for h_entry in browser_history_data:
                entry: dict = h_entry

                # Her giriş için gerekli bilgileri al
                favicon: str = entry.get("favicon", "")
                link: str = entry.get("url", "")
                title: str = entry.get("title", "")
                time: int = entry.get("timestamp", "")

                favicon = favicon.replace("'", "\\'")
                link = link.replace("'", "\\'")
                title = title.replace("'", "\\'")

                # JavaScript kodunu oluştur ve çalıştır
                js_code = f"add_history_entry('{favicon}', '{link}', '{title}', {time});"
                view.page().runJavaScript(js_code)
        elif url.startswith("root/new_tab"):
            history_file_path = os.path.join(_browser_profile_dir, "history.json")

            with open(history_file_path, "r") as file:
                browser_history_data: list = json.load(file)

            view.page().settings().setAttribute(QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, True)

            for history_item in browser_history_data[0:3]:
                entry: dict = history_item

                # Her giriş için gerekli bilgileri al
                favicon: str = entry.get("favicon", "")
                link: str = entry.get("url", "")

                favicon = favicon.replace("'", "\\'")
                link = link.replace("'", "\\'")

                js_code = f"add_history_entry('{favicon}', '{link}');"
                view.page().runJavaScript(js_code)
        else:
            view.page().settings().setAttribute(QWebEngineSettings.WebAttribute.LocalContentCanAccessRemoteUrls, False)
            
    def addHistoryDataFromWidget(self, view: QWebEngineView):
        global _browser_profile_dir
        global _browser_history_data

        print("[INFO] addHistoryDataFromWidget called.")
        print("[INFO] addHistoryDataFromWidget: url is", AluminiumEngine.Scheme.apply(view.url().toString()))

        if (AluminiumEngine.Scheme.apply(view.url().toString()).startswith("aluminium") == False) and (
                AluminiumEngine.Scheme.apply(view.url().toString()).__contains__("history") == False):
            current_tab_widget: QWebEngineView = view
            url = current_tab_widget.url().toString()
            title = current_tab_widget.title()

            print("[INFO] addHistoryDataFromWidget: view.icon().availableSizes() is", view.icon().availableSizes())
            sizes = view.icon().availableSizes()
            if sizes != []:
                favicon_pixmap = view.icon().pixmap(sizes[-1])

                # QPixmap'i 16x16 boyutuna dönüştür
                favicon_16x16 = favicon_pixmap.scaled(72, 72)

                # Dönüştürülen QPixmap'i base64 formatına dönüştür
                byte_array = QByteArray()
                buffer = QBuffer(byte_array)
                buffer.open(QIODevice.WriteOnly)
                favicon_16x16.save(buffer, "PNG")
                base64_favicon = str(byte_array.toBase64().data()).replace("b'", "", 1)[:-1]
            else:
                base64_favicon = ""

            # Oluşturulan girişin tarih ve zaman bilgisi
            timestamp = int(datetime.now().timestamp())

            # Oluşturulan girişin sözlük formatında hazırlanması
            history_entry = {
                "url": url,
                "title": title,
                "favicon": base64_favicon,
                "timestamp": timestamp
            }

            # Geçmiş verilerinin dosya yolu oluşturuluyor
            history_file_path = os.path.join(_browser_profile_dir, "history.json")

            # Yeni geçmiş girişinin eklenmesi
            browser_history_data: list = _browser_history_data

            browser_history_data.insert(0, history_entry)

            # Yeni geçmiş verilerinin dosyaya yazılması
            with open(history_file_path, "w") as file:
                json.dump(browser_history_data, file)

            with open(history_file_path, "r") as file:
                _browser_history_data = json.load(file)

    def closeTab(self, index):
        # Close the tab and delete the associated webview
        widget = self.tabWidget.widget(index)
        widget.deleteLater()
        self.tabWidget.removeTab(index)

        if self.browser_tab_bar.count() > 1:
            self.browser_tab_bar.show()
            self.content.raise_()
        else:
            self.browser_tab_bar.hide()
            self.headerBar.raise_()

        if self.browser_tab_bar.count() == 0:
            self.close()

    def tabChanged(self, index):
        # Update the URL box when the active tab changes
        current_tab_widget: QWebEngineView = self.tabWidget.currentWidget()
        if current_tab_widget:
            link = AluminiumEngine.Scheme.apply(current_tab_widget.url().toString())
            if link in self.non_visible_aluminium_scheme_pages:
                link = ""
            self.url_box.setText(link)
            self.setWindowTitle(current_tab_widget.title())
            if self.inspector != None:
                current_tab_widget.page().setDevToolsPage(self.inspector.page())

    def titleChanged(self, widget):
        current_tab_widget: QWebEngineView = self.tabWidget.currentWidget()
        if widget == current_tab_widget:
            self.setWindowTitle(current_tab_widget.title() + self.TitleEnd)

    def onFeaturePermissionRequested(self, url, feature, view):
        aurl = AluminiumEngine.Scheme.apply(view.url().toString()).replace("aluminium:", "", 1)
        if aurl.startswith("//"):
            aurl = aurl.replace("//", "root/", 1)
        elif aurl.startswith("/"):
            aurl = aurl.replace("/", "root/", 1)
        else:
            aurl = "root/" + aurl

        if aurl.startswith("root/new_tab"):
            self.tabWidget.currentWidget().page().setFeaturePermission(url, feature,
                                                                       QWebEnginePage.PermissionGrantedByUser)
        else:
            denybtn = QPushButton("Deny", self)
            acceptbtn = QPushButton("Accept", self)
            tbdiag = QToolBar("warn_dialog")
            denybtn.clicked.connect(
                lambda _: self.tabWidget.currentWidget().page().setFeaturePermission(url, feature,
                                                                                     QWebEnginePage.PermissionDeniedByUser))
            acceptbtn.clicked.connect(
                lambda _: self.tabWidget.currentWidget().page().setFeaturePermission(url, feature,
                                                                                     QWebEnginePage.PermissionGrantedByUser))
            denybtn.clicked.connect(lambda _: tbdiag.deleteLater())
            acceptbtn.clicked.connect(lambda _: tbdiag.deleteLater())

            print(feature)
            if feature == QWebEnginePage.Feature.Notifications:
                self.warnMessageBanner(str(url.toString()) + " wants send notifications.", acceptbtn, denybtn, tbdiag)
            elif feature == QWebEnginePage.Feature.Geolocation:
                self.warnMessageBanner(str(url.toString()) + " wants know your location.", acceptbtn, denybtn, tbdiag)
            elif feature == QWebEnginePage.Feature.MediaVideoCapture:
                self.warnMessageBanner(str(url.toString()) + " wants use the webcam.", acceptbtn, denybtn, tbdiag)
            elif feature == QWebEnginePage.Feature.MediaAudioCapture:
                self.warnMessageBanner(str(url.toString()) + " wants use the microphone.", acceptbtn, denybtn, tbdiag)
            elif feature == QWebEnginePage.Feature.MediaAudioVideoCapture:
                self.warnMessageBanner(str(url.toString()) + " wants use the webcam and microphone.", acceptbtn,
                                       denybtn, tbdiag)
            elif feature == QWebEnginePage.Feature.MouseLock:
                self.tabWidget.currentWidget().page().setFeaturePermission(url, feature,
                                                                           QWebEnginePage.PermissionGrantedByUser)
            elif feature == QWebEnginePage.Feature.DesktopVideoCapture or feature == QWebEnginePage.Feature.DesktopAudioVideoCapture:
                self.warnMessageBanner(str(url.toString()) + " wants record/stream your screen.", acceptbtn, denybtn,
                                       tbdiag)
            else:
                self.warnMessageBanner(str(url.toString()) + " wants use a feature of the browser.", acceptbtn, denybtn,
                                       tbdiag)

    def warnMessageBanner(self, message, acceptbtn, denybtn, tbdiag):
        message = "     " + message
        tbdiag.setContextMenuPolicy(Qt.PreventContextMenu)
        tbdiag.setObjectName("warn-banner")
        titlelabel = QLabel(message)
        font = titlelabel.font()
        font.setBold(True)
        titlelabel.setFont(font)
        spaceItem = QWidget()
        spaceItem.setSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.Fixed)
        tbdiag.addWidget(titlelabel)
        tbdiag.addWidget(spaceItem)
        tbdiag.addWidget(denybtn)
        tbdiag.addWidget(acceptbtn)
        tbdiag.layout().setSpacing(8)
        tbdiag.setMovable(False)
        tbdiag.setStyleSheet("""
            QToolBar {
                background: #6BCDCE;
            }
            QPushButton, QToolButton {
                background: #FFFFFF;
                border: none;
                color: #6BCDCE;
            }
            
            QPushButton:hover, QToolButton:hover {
                background: #66FFFFFF;
                border: none;
                color: #6BCDCE;
            }
        """)
        self.addToolBar(Qt.ToolBarArea.BottomToolBarArea, tbdiag)

    def url_box_update(self, widget):
        current_tab_widget: QWebEngineView = self.tabWidget.currentWidget()
        if widget == current_tab_widget:
            link = AluminiumEngine.Scheme.apply(current_tab_widget.url().toString())
            if link in self.non_visible_aluminium_scheme_pages:
                link = ""
            self.url_box.setText(link)

        if current_tab_widget.page().history().canGoBack():
            self.back_btn.setEnabled(True)
        else:
            self.back_btn.setEnabled(False)

        if current_tab_widget.page().history().canGoForward():
            self.next_btn.setEnabled(True)
        else:
            self.next_btn.setEnabled(False)

    suggestions_fetched = Signal(list)

    def fetch_search_suggestions(self):
        # Get the current text in the URL box
        query = self.url_box.text()

        if query:
            try:
                search_engine_data = libSearchEngine.get_engine(self.search_engine)
                url = search_engine_data["suggestion_link"]
                params = search_engine_data["params"]
                params[search_engine_data["query_param"]] = query
                response = requests.get(url, params=params, stream=True)

                # Check if the request was successful (status code 200)
                response.raise_for_status()

                # Process the streaming response and extract suggestions
                decoded_chunks = []
                for chunk in response.iter_content(chunk_size=128):
                    # Decode each chunk using 'latin-1'
                    decoded_chunk = chunk.decode("latin-1")
                    decoded_chunks.append(decoded_chunk)

                    # Process events to keep the GUI responsive
                    QCoreApplication.processEvents()

                # Concatenate decoded chunks and parse as JSON
                concatenated_data = ''.join(decoded_chunks)
                print(json.loads(concatenated_data))
                suggestions = libSearchEngine.get_suggestions(self.search_engine, json.loads(concatenated_data))

                return suggestions

            except requests.exceptions.RequestException as e:
                print(f"Error fetching search suggestions: {e}")

    def fetch_search_suggestions_threaded(self):
        # Create a separate thread for network request
        threading.Thread(target=self.fetch_search_suggestions_thread).start()

    def fetch_search_suggestions_thread(self):
        # Fetch suggestions using streaming in a separate thread
        suggestions = self.fetch_search_suggestions()

        # Update the completer with the suggestions
        if suggestions:
            # Use a signal to communicate with the main thread
            self.suggestions_fetched.emit(suggestions)

    def fetchSearchSuggestions(self):
        # Connect the signal to update the completer in the main thread
        self.suggestions_fetched.connect(self.update_completer)

        # Fetch suggestions in a separate thread
        self.fetch_search_suggestions_threaded()

    def closeEvent(self, event):
        super().closeEvent(event)
        self.settingsprof.setValue("winSize", self.size())
        self.settingsprof.setValue("winPos", self.pos())
        if self.isPrivate:
            self.profile.clearHttpCache()
            self.profile.clearAllVisitedLinks()

    def update_completer(self, suggestions):
        # Update the completer with the suggestions in the main thread
        model = QStringListModel(suggestions, self.completer)
        self.completer.setModel(model)

    def handleSuggestionActivated(self, index):
        # Handle the activated suggestion
        selected_item = self.completer.currentCompletion()
        query = f"{libSearchEngine.get_engine(self.search_engine)['search_html']}{selected_item.replace(' ', '+')}"
        self.tabWidget.currentWidget().load(QUrl(query))

    def updateLoadingState(self, is_loading):
        # Update the loading state and change the button icon
        self.loading = is_loading
        if is_loading:
            self.refresh_btn.setIcon(QIcon.fromTheme("close-symbolic"))
        else:
            self.refresh_btn.setIcon(QIcon.fromTheme("view-refresh"))

    def handleSearchOrLoad(self):
        current_tab_widget = self.tabWidget.currentWidget()
        if current_tab_widget:
            input_text = self.url_box.text()

            if self.is_valid_url(input_text) or input_text.startswith(("http", "https")):
                # Valid URL or starts with a scheme, load it
                current_tab_widget.load(QUrl.fromUserInput(AluminiumEngine.Scheme.unapply(input_text)))
            elif "." in input_text:
                # Contains a dot, assume it's a domain and prepend "https://"
                current_tab_widget.load(QUrl(f"https://{input_text}"))
            else:
                # Not a valid URL, perform a search
                search_query = f"{libSearchEngine.get_engine(self.search_engine)['search_html']}{input_text.replace(' ', '+')}"
                current_tab_widget.load(QUrl(search_query))

    def is_valid_url(self, text):
        try:
            result = urlparse(text)
            global VALID_SCHEMES
            return all([result.scheme.lower() in VALID_SCHEMES])
        except ValueError:
            return False

    def handleRefresh(self):
        current_tab_widget = self.tabWidget.currentWidget()

        if self.loading:
            current_tab_widget.stop()  # Stop loading
        else:
            current_tab_widget.reload()  # Reload the page


if __name__ == "__main__":
    if sys.argv.__contains__("--wayland") == False:
        if os.environ["XDG_SESSION_TYPE"].lower().__contains__("wayland"):
            print("[NebiAluminium] INFO: Wayland detected. Disabling due bugs.")
            os.environ["QT_QPA_PLATFORM"] = "xcb"
    elif sys.argv.__contains__("--wayland"):
        print("[NebiAluminium] WARNING: Wayland support is experimental and won't work on NVIDIA driver.")
    
    _browser_profile_dir = os.path.expanduser("~/.local/share/NebiSoft/NebiAluminium/Profiles/Default")
    profile_dir = _browser_profile_dir
    if not os.path.exists(profile_dir):
        os.makedirs(profile_dir)

    history_file = os.path.join(profile_dir, "history.json")
    if not os.path.exists(history_file):
        _browser_history_data = []
        with open(history_file, "w") as file:
            json.dump(_browser_history_data, file)

    with open(history_file, "r") as file:
        _browser_history_data = json.load(file)

    bookmarks_file = os.path.join(profile_dir, "bookmarks.json")
    if not os.path.exists(bookmarks_file):
        _browser_bookmark_data = {}
        with open(bookmarks_file, "w") as file:
            json.dump(_browser_bookmark_data, file)

    with open(bookmarks_file, "r") as file:
        _browser_bookmark_data = json.load(file)

    app = QApplication(sys.argv)
    font = EzFont("lib/QtNebiStylestrip/fonts/Manrope/Manrope-Medium.ttf")
    app.setFont(font)
    app.setApplicationName("NebiAluminium")
    QSS.load_stylesheet(app, "lib/QtNebiStylestrip/qss/app.qss")

    app.setApplicationName("NebiAluminium")
    app.setApplicationDisplayName("NebiAluminium")
    app.setOrganizationName("NebiSoft")

    QIcon.setThemeSearchPaths(["/usr/share/icons"])
    QIcon.setThemeName("Flat-Skeuomorphism")

    icon = QIcon.fromTheme('back')
    print("[INFO] Icon Theme: " + str(icon.themeName()))

    font = EzFont("lib/QtNebiStylestrip/fonts/Manrope/Manrope-Medium.ttf")
    font.setPointSize(10)
    app.setFont(font)

    # Create and show the window
    window = NebiAluminium()
    window.show()

    # Run the application
    sys.exit(app.exec())
