import os

ENGINE_PATH = f"{os.getcwd()}/engine/html/"

class Scheme:
    def apply(link):
        schf = f"{link.replace('file://' + ENGINE_PATH, 'aluminium:', 1).replace('/index.html', '')}"
        return schf

    def unapply(link):
        if link.startswith('aluminium:'):
            schf = f"{link.replace('aluminium:', 'file://' + ENGINE_PATH, 1)}/index.html"
        else:
            schf = link
        return schf
