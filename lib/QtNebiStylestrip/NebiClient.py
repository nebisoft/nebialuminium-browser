from PySide6.QtWidgets import *
from lib.QtNebiStylestrip.QSS import *
from lib.QtNebiStylestrip.HeaderBar import *
from PySide6 import QtCore, QtWidgets, QtGui
import lib.EzPlatform as EzPlatform


class CornerGrip(QSizeGrip):
    def paintEvent(self, event):
        super().paintEvent(event)
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)
        painter.setPen(Qt.NoPen)
        painter.setBrush(QColor(0, 0, 0, 0))  # Transparent brush
        painter.drawRect(self.rect())


class SideGrip(QtWidgets.QWidget):
    def __init__(self, parent, edge):
        QtWidgets.QWidget.__init__(self, parent)
        self.systemEdge = edge
        if edge == QtCore.Qt.LeftEdge:
            self.setCursor(QtCore.Qt.SizeHorCursor)
            self.resizeFunc = self.resizeLeft
        elif edge == QtCore.Qt.TopEdge:
            self.setCursor(QtCore.Qt.SizeVerCursor)
            self.resizeFunc = self.resizeTop
        elif edge == QtCore.Qt.RightEdge:
            self.setCursor(QtCore.Qt.SizeHorCursor)
            self.resizeFunc = self.resizeRight
        else:
            self.setCursor(QtCore.Qt.SizeVerCursor)
            self.resizeFunc = self.resizeBottom
        self.mousePos = None

    def resizeLeft(self, delta):
        window = self.window()
        width = max(window.minimumWidth(), window.width() - delta.x())
        geo = window.geometry()
        geo.setLeft(geo.right() - width)
        window.setGeometry(geo)

    def resizeTop(self, delta):
        window = self.window()
        height = max(window.minimumHeight(), window.height() - delta.y())
        geo = window.geometry()
        geo.setTop(geo.bottom() - height)
        window.setGeometry(geo)

    def resizeRight(self, delta):
        window = self.window()
        width = max(window.minimumWidth(), window.width() + delta.x())
        window.resize(width, window.height())

    def resizeBottom(self, delta):
        window = self.window()
        height = max(window.minimumHeight(), window.height() + delta.y())
        window.resize(window.width(), height)

    # def mousePressEvent(self, event):
    #     if event.button() == QtCore.Qt.LeftButton:
    #         self.mousePos = event.pos()

    def mousePressEvent(self, event):
        self.window().startSystemResize(self.systemEdge)

    def mouseReleaseEvent(self, event):
        self.mousePos = None


class NebiClient(QMainWindow):
    _gripSize = 10
    if EzPlatform.get_platform()["desktop"].__contains__("nebide"):
        if not EzPlatform.get_platform()["desktop"] == "nebide_wayland":
            _gripSize = 1

    def __init__(self):
        super().__init__()
        self.setWindowFlags(Qt.WindowFlags.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)
        QSS.load_stylesheet(self, "lib/QtNebiStylestrip/qss/app.qss")
        
        self.widget = QWidget(self)
        self.widget.setObjectName("NebiClient")
        layout = QVBoxLayout()
        layout.setSpacing(0)
        self.widget.setLayout(layout)
        layout.setContentsMargins(*[0] * 4)
        self.headerBar = HeaderBar(self, self._gripSize)
        layout.addWidget(self.headerBar, 0, alignment=Qt.AlignTop)
        self.content = QWidget(self)
        self.content_layout = QVBoxLayout()
        self.menu_bar = QMenuBar()
        QSS.load_stylesheet(self.menu_bar, "lib/QtNebiStylestrip/qss/app.qss")
        self.content_layout.addWidget(self.menu_bar)
        self.content.setLayout(self.content_layout)
        layout.addWidget(self.content, 3)
        
        self.setCentralWidget(self.widget)

        self.sideGrips = [
            SideGrip(self, QtCore.Qt.LeftEdge),
            SideGrip(self, QtCore.Qt.TopEdge),
            SideGrip(self, QtCore.Qt.RightEdge),
            SideGrip(self, QtCore.Qt.BottomEdge),
        ]
        # corner grips should be "on top" of everything, otherwise the side grips
        # will take precedence on mouse events, so we are adding them *after*;
        # alternatively, widget.raise_() can be used
        self.cornerGrips = [CornerGrip(self) for i in range(4)]

    def applyFontToWidgets(self, layout):
        for i in range(layout.count()):
            item = layout.itemAt(i)
            if isinstance(item, QLayout):
                self.applyFontToWidgets(item)
            elif isinstance(item.widget(), QWidget):
                item.widget().setFont(self.font())

    @property
    def gripSize(self):
        return self._gripSize

    def setGripSize(self, size):
        if size == self._gripSize:
            return
        self._gripSize = max(0, size)
        self.updateGrips()

    def updateGrips(self):
        if self.isFullScreen() or self.isMaximized():
            self.setContentsMargins(0, 0, 0, 0)
        else:
            self.setContentsMargins(*[self.gripSize] * 4)

        outRect = self.rect()
        # an "inner" rect used for reference to set the geometries of size grips
        inRect = outRect.adjusted(self.gripSize, self.gripSize,
                                  -self.gripSize, -self.gripSize)

        # top left
        self.cornerGrips[0].setGeometry(
            QtCore.QRect(outRect.topLeft(), inRect.topLeft()))
        # top right
        self.cornerGrips[1].setGeometry(
            QtCore.QRect(outRect.topRight(), inRect.topRight()).normalized())
        # bottom right
        self.cornerGrips[2].setGeometry(
            QtCore.QRect(inRect.bottomRight(), outRect.bottomRight()))
        # bottom left
        self.cornerGrips[3].setGeometry(
            QtCore.QRect(outRect.bottomLeft(), inRect.bottomLeft()).normalized())

        # left edge
        self.sideGrips[0].setGeometry(
            0, inRect.top(), self.gripSize, inRect.height())
        # top edge
        self.sideGrips[1].setGeometry(
            inRect.left(), 0, inRect.width(), self.gripSize)
        # right edge
        self.sideGrips[2].setGeometry(
            inRect.left() + inRect.width(),
            inRect.top(), self.gripSize, inRect.height())
        # bottom edge
        self.sideGrips[3].setGeometry(
            self.gripSize, inRect.top() + inRect.height(),
            inRect.width(), self.gripSize)

        for grip in self.cornerGrips:
            grip.setGraphicsEffect(QtWidgets.QGraphicsOpacityEffect(opacity=0.0))
            grip.setProperty("hide", True)
        for grip in self.sideGrips:
            grip.setGraphicsEffect(QtWidgets.QGraphicsOpacityEffect(opacity=0.0))

    def startSystemResize(self, edges):
        if edges & Qt.LeftEdge:
            self.windowHandle().startSystemResize(Qt.LeftEdge)
        elif edges & Qt.TopEdge:
            self.windowHandle().startSystemResize(Qt.TopEdge)
        elif edges & Qt.RightEdge:
            self.windowHandle().startSystemResize(Qt.RightEdge)
        elif edges & Qt.BottomEdge:
            self.windowHandle().startSystemResize(Qt.BottomEdge)

    def resizeEvent(self, event):
        QtWidgets.QMainWindow.resizeEvent(self, event)
        self.updateGrips()
        if self.menu_bar != None and self.menu_bar.width() != 0:
            self.menu_bar.setMaximumSize(self.width(), self.height())
            
    def draw_shadow(self, painter, margin, radius, start_color, end_color, start_position, end_position0, end_position1, width, height):
        painter.setPen(Qt.NoPen)

        gradient = QLinearGradient()
        gradient.setColorAt(start_position, start_color)
        gradient.setColorAt(end_position0, end_color)

        # Right
        right0 = QPointF(width - margin, height / 2)
        right1 = QPointF(width - 4, height / 2)
        gradient.setStart(right0)
        gradient.setFinalStop(right1)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(QPointF(width - margin * radius, margin), QPointF(width, height - margin)), 0.0, 0.0)

        # Left
        left0 = QPointF(margin, height / 2)
        left1 = QPointF(4, height / 2)
        gradient.setStart(left0)
        gradient.setFinalStop(left1)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(QPointF(margin * radius, margin), QPointF(0, height - margin)), 0.0, 0.0)

        # Top
        top0 = QPointF(width / 2, margin)
        top1 = QPointF(width / 2, 4)
        gradient.setStart(top0)
        gradient.setFinalStop(top1)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(QPointF(width - margin, 0), QPointF(margin, margin)), 0.0, 0.0)

        # Bottom
        bottom0 = QPointF(width / 2, height - margin)
        bottom1 = QPointF(width / 2, height)
        gradient.setStart(bottom0)
        gradient.setFinalStop(bottom1)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(QPointF(margin, height - margin), QPointF(width - margin, height)), 0.0, 0.0)

        # BottomRight
        bottomright0 = QPointF(width - margin, height - margin)
        bottomright1 = QPointF(width - 6, height)
        gradient.setStart(bottomright0)
        gradient.setFinalStop(bottomright1)
        gradient.setColorAt(end_position1, end_color)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(bottomright0, bottomright1), 0.0, 0.0)

        # BottomLeft
        bottomleft0 = QPointF(margin, height - margin)
        bottomleft1 = QPointF(6, height)
        gradient.setStart(bottomleft0)
        gradient.setFinalStop(bottomleft1)
        gradient.setColorAt(end_position1, end_color)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(bottomleft0, bottomleft1), 0.0, 0.0)

        # TopLeft
        topleft0 = QPointF(margin, margin)
        topleft1 = QPointF(6, 6)
        gradient.setStart(topleft0)
        gradient.setFinalStop(topleft1)
        gradient.setColorAt(end_position1, end_color)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(topleft0, topleft1), 0.0, 0.0)

        # TopRight
        topright0 = QPointF(width - margin, margin)
        topright1 = QPointF(width - 6, 6)
        gradient.setStart(topright0)
        gradient.setFinalStop(topright1)
        gradient.setColorAt(end_position1, end_color)
        painter.setBrush(QBrush(gradient))
        painter.drawRoundedRect(QRectF(topright0, topright1), 0.0, 0.0)

        # Widget
        painter.setBrush(QBrush("#FFFFFF"))
        painter.setRenderHint(QPainter.Antialiasing)
        painter.drawRoundedRect(QRectF(QPointF(margin, margin), QPointF(width - margin, height - margin)), radius, radius)
        painter.end()
            
    def paintEvent(self, event):
        painter = QPainter(self)
        if not self.isFullScreen() or not self.isMaximized():
            self.draw_shadow(
                painter,
                margin=10,
                radius=10.0,
                start_color=QColor(0, 0, 0, 64),
                end_color=QColor(0, 0, 0, 0),
                start_position=0.0,
                end_position0=1.0,
                end_position1=0.6,
                width=self.width(),
                height=self.height()
            )

