import os
from PySide6.QtGui import QFont, QFontDatabase


class EzFont(QFont):
    def __init__(self, font_path):
        super().__init__()

        # Adjust file path based on the operating system
        if os.name == 'nt':  # 'nt' represents Windows
            prefix = ""
        else:
            # For non-Windows platforms, use the current working directory as the prefix
            prefix = os.getcwd()

        adjusted_font_path = os.path.join(prefix, font_path)

        # Load font from adjusted file path
        font_id = QFontDatabase.addApplicationFont(adjusted_font_path)

        if font_id == -1:
            raise ValueError(f"Error loading font from file: {adjusted_font_path}")

        # Retrieve font family
        font_info = QFontDatabase.applicationFontFamilies(font_id)

        if not font_info:
            raise ValueError(f"No font families found in file: {adjusted_font_path}")

        self.setFamily(font_info[0])
