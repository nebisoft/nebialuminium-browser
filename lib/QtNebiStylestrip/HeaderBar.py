from PySide6.QtGui import *
from PySide6.QtWidgets import *
from lib.QtNebiStylestrip.QSS import *
from lib.QtNebiStylestrip.EzFont import *
import lib.EzPlatform as EzPlatform
import os

class HeaderBar(QWidget):
    def mouseDoubleClickEvent(self, event):
        if event.button() == Qt.LeftButton:
            if self.parent.isMaximized():
                self.parent.showNormal()
            else:
                self.parent.showMaximized()

    def __init__(self, parent, gripSize):
        super().__init__()
        QSS.load_stylesheet(self, "lib/QtNebiStylestrip/qss/app.qss")
        self.bg = "#ffFFFFFF"

        self.parent = parent
        self.gripSize = gripSize

        # Create a QVBoxLayout for the entire widget
        self.layout = QVBoxLayout(self)
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.layout.setSpacing(0)

        # Create a sub-widget for the header content
        self.headerContent = QWidget(self)
        self.headerContent.setObjectName("header")
        self.headerLayout = QHBoxLayout(self.headerContent)
        self.headerLayout.setContentsMargins(16, 12, 16, 12)
        self.headerLayout.setSpacing(16)

        self.title = QWidget()
        self.title.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)
        self.header_title_layout = QHBoxLayout()
        self.header_title_layout.setContentsMargins(0, 0, 0, 0)
        self.title.setLayout(self.header_title_layout)

        self.title_text = QLabel(self.parent.windowTitle())  # Initialize with the window title
        font = EzFont("lib/QtNebiStylestrip/fonts/Manrope/Manrope-ExtraBold.ttf")
        font.setPointSize(10)  # Punto boyutunu ayarla
        font.setBold(True)
        self.title_text.setFont(font)
        self.header_title_layout.addWidget(self.title_text, alignment=Qt.AlignCenter, stretch=1)

        btn_size = 24

        # Close button
        self.btn_close = QLabel()
        close_pixmap = QPixmap("lib/QtNebiStylestrip/assets/titlebutton-close.png")
        close_hover_pixmap = QPixmap("lib/QtNebiStylestrip/assets/titlebutton-close-hover.png")
        self.set_button_image(self.btn_close, close_pixmap, btn_size)
        self.btn_close.mousePressEvent = self.btn_close_clicked
        self.btn_close.enterEvent = lambda event: self.set_button_image(self.btn_close, close_hover_pixmap, btn_size)
        self.btn_close.leaveEvent = lambda event: self.set_button_image(self.btn_close, close_pixmap, btn_size)

        # Minimize button
        self.btn_minimize = QLabel()
        minimize_pixmap = QPixmap("lib/QtNebiStylestrip/assets/titlebutton-minimize.png")
        minimize_hover_pixmap = QPixmap("lib/QtNebiStylestrip/assets/titlebutton-minimize-hover.png")
        self.set_button_image(self.btn_minimize, minimize_pixmap, btn_size)
        self.btn_minimize.mousePressEvent = self.btn_minimize_clicked
        self.btn_minimize.enterEvent = lambda event: self.set_button_image(self.btn_minimize, minimize_hover_pixmap, btn_size)
        self.btn_minimize.leaveEvent = lambda event: self.set_button_image(self.btn_minimize, minimize_pixmap, btn_size)

        self.title_text.setAlignment(Qt.AlignCenter)

        # Adjusted layout for the desired format
        self.headerLayout.addWidget(self.btn_close)
        self.headerLayout.addWidget(self.title)
        self.headerLayout.addWidget(self.btn_minimize)

        # Add the header content widget to the main layout
        self.layout.addWidget(self.headerContent)

        self.start = QPoint(0, 0)
        self.pressing = False

        self.topLeftCorner = True
        self.topRightCorner = True
        self.bottomRightCorner = False
        self.bottomLeftCorner = False
        self.radius = 14

        # Connect the windowTitleChanged signal to update the title label
        self.parent.windowTitleChanged.connect(self.update_title_label)

    def update_title_label(self, new_title):
        try:
            self.title_text.setText(new_title)
        except:
            pass

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.setRenderHint(QPainter.Antialiasing)

        # Start position
        start_pos = QPointF(0, self.height() / 2)

        path = QPainterPath()
        path.moveTo(start_pos)

        # Top left corner
        if self.topLeftCorner:
            path.lineTo(0, self.radius)
            path.arcTo(0, 0, self.radius * 2, self.radius * 2, 180, -90)
        else:
            path.lineTo(0, 0)

        # Top right corner
        if self.topRightCorner:
            path.lineTo(self.width() - self.radius, 0)
            path.arcTo(self.width() - self.radius * 2, 0, self.radius * 2, self.radius * 2, 90, -90)
        else:
            path.lineTo(self.width(), 0)

        # Bottom right corner
        if self.bottomRightCorner:
            path.lineTo(self.width(), self.height() - self.radius)
            path.arcTo(self.width() - self.radius * 2, self.height() - self.radius * 2, self.radius * 2, self.radius * 2, 0, -90)
        else:
            path.lineTo(self.width(), self.height())

        # Bottom left corner
        if self.bottomLeftCorner:
            path.lineTo(self.radius, self.height())
            path.arcTo(0, self.height() - self.radius * 2, self.radius * 2, self.radius * 2, 270, -90)
        else:
            path.lineTo(0, self.height())

        # Close path
        path.lineTo(start_pos)

        # Draw background
        painter.fillPath(path, QColor(self.bg))

    def setBackgroundColor(self, color):
        self.bg = color
        self.repaint()

    def mousePressEvent(self, event):
        if event.buttons() == Qt.LeftButton:
            self.parent.windowHandle().startSystemMove()

    def btn_close_clicked(self, event):
        self.parent.close()

    def btn_minimize_clicked(self, event):
        self.parent.showMinimized()

    def set_button_image(self, label, pixmap, size):
        label.setPixmap(pixmap.scaled(size, size, Qt.AspectRatioMode.KeepAspectRatio, Qt.SmoothTransformation))
