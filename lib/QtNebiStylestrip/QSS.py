from PySide6.QtCore import *

class QSS:
    def load_stylesheet(wid, filename):
        style_file = QFile(filename)
        if style_file.open(QFile.ReadOnly | QFile.Text):
            stream = QTextStream(style_file)
            wid.setStyleSheet(stream.readAll())
