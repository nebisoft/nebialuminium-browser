EngineDB = {
    "searxng": {
        "suggestion_link": "https://searx.be/autocompleter",
        "params": {},
        "query_param": "q",
        "search_html": "https://searx.be/search?q="
    },
    "duckduckgo": {
        "suggestion_link": "https://duckduckgo.com/ac/",
        "params": {"output": "firefox"},
        "query_param": "q",
        "search_html": "https://duckduckgo.com/?q="
    },
    "google": {
        "suggestion_link": "http://suggestqueries.google.com/complete/search",
        "params": {"output": "firefox"},
        "query_param": "q",
        "search_html": "https://google.com/search?q="
    },
}

class libSearchEngine:
    def get_engine(name:str):
        pretty_name = name.lower()
        global EngineDB
        try:
            return EngineDB[pretty_name]
        except:
            return {
                "suggestion_link": None,
                "params": None,
                "query_param": None,
                "search_html": name
            },

    def get_suggestions(engine_name:str, json_data):
        if engine_name.lower() == "searxng":
            return json_data[1]
        if engine_name.lower() == "duckduckgo":
            return [result["phrase"] for result in json_data]
        elif engine_name.lower() == "google":
            return json_data[1]
        else:
            return ["Unknown Search Engine"]
