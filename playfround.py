from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *
from PySide6.QtWebEngineCore import *
from PySide6.QtWebEngineWidgets import *

class LockedQGraphicsView(QGraphicsView):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self.locked = False
		self.lockRect = QRectF()
		self.lockTimer = QTimer()
		self.lockTimer.timeout.connect(self.checkMousePosition)

	def lockMouse(self):
		self.locked = True
		self.lockRect = self.viewport().rect()
		self.lockTimer.start(10)  # Check position every 10 ms

	def unlockMouse(self):
		self.locked = False
		self.lockTimer.stop()

	def checkMousePosition(self):
		if self.locked:
			cursor_pos = self.mapFromGlobal(QCursor.pos())
			if not self.lockRect.contains(cursor_pos):
				center = self.lockRect.center()
				QCursor.setPos(self.mapToGlobal(center.toPoint()))

	def mouseMoveEvent(self, event):
		if self.locked:
			cursor_pos = self.mapFromGlobal(QCursor.pos())
			if not self.lockRect.contains(cursor_pos):
				return  # Ignore event if cursor is out of bounds
		super().mouseMoveEvent(event)

	def mousePressEvent(self, event):
		if event.button() == Qt.RightButton:
			if self.locked:
				self.unlockMouse()
			else:
				self.lockMouse()
		super().mousePressEvent(event)

def onFeaturePermissionRequested(url, feature, view):
	view.page().setFeaturePermission(url, feature, QWebEnginePage.PermissionGrantedByUser)
	
def main():
	app = QApplication([])

	scene = QGraphicsScene()
	view = LockedQGraphicsView(scene)


	proxy_widget = QGraphicsProxyWidget()
	
	web_view = QWebEngineView(view.window())
	web_view.page().featurePermissionRequested.connect(lambda url, feature: onFeaturePermissionRequested(url, feature, web_view))
	web_view.setUrl(QUrl("https://www.krunker.io"))
	
	proxy_widget.setWidget(web_view)
	scene.addItem(proxy_widget)

	view.setScene(scene)
	view.show()

	app.exec_()

if __name__ == "__main__":
	main()

