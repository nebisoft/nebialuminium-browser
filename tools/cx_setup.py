import sys
from cx_Freeze import setup, Executable

# Dependencies are automatically detected, but it might need fine tuning.
build_exec_options = {
    "excludes"			: ["tkinter", "unittest"],
    "zip_include_packages"	: ["requests", "PySide6"],
    "include_files"		: ["lib", "engine", "assets"],
    "optimize"			: 2,
}
setup(
    name="NebiAluminium",
    version="3.0",
    description="NebiAluminium",
    options={"build_exe": build_exec_options},
    executables=[Executable("NebiAluminium.py")],
)

